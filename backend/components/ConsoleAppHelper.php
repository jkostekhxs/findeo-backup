<?php

namespace backend\components;

/**
 * Class ConsoleAppHelper
 * @package backend\components
 */
class ConsoleAppHelper
{

    public static function runAction( $route, $params_act = [ ], $controllerNamespace = null )
    {

        $oldApp = \Yii::$app;

        // fcgi doesn't have STDIN and STDOUT defined by default
        defined( 'STDIN' ) or define( 'STDIN', fopen( 'php://stdin', 'r' ) );
        defined( 'STDOUT' ) or define( 'STDOUT', fopen( 'php://stdout', 'w' ) );

        /** @noinspection PhpIncludeInspection */
        //$config = require( \Yii::getAlias( '@backend/config/console.php' ) );
        $config = require( \Yii::getAlias( '@console/config/main.php' ) );

        $consoleApp = new \yii\console\Application( $config );

        if (!is_null( $controllerNamespace )) {
            $consoleApp->controllerNamespace = $controllerNamespace;
        }

        try {

            // use current connection to DB
            \Yii::$app->set( 'db', $oldApp->db );

            ob_start();

            $exitCode = $consoleApp->runAction(
                $route,
                array_merge( $params_act, [ 'interactive' => false ] )
            );

            $result = ob_get_clean();

            \Yii::trace( $result, 'console' );

        } catch ( \Exception $e ) {

            \Yii::warning( $e->getMessage() . ' in ' . $e->getFile() . ' on line ' . $e->getLine(), 'console' );

            $exitCode = 1;
        }

        \Yii::$app = $oldApp;

        return ['code' => $exitCode, 'result' => $result];
    }
}


?>