<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "partners".
 *
 * @property integer $id
 * @property string $name
 * @property string $street
 * @property string $postcode
 * @property string $city
 * @property integer $tax_num
 * @property string $invoice_email
 * @property string $contact_email
 * @property string $lang
 * @property integer $added_by
 * @property string $added_on
 */
class Partners extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'partners';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                ['name', 'invoice_email', 'contact_email'],
                'required',
                'message'=>'Prosze uzupełnić pole: {attribute}.'
            ],
            [['tax_num'], 'integer'],
            [['added_by'], 'string'],
            [['added_on','added_by_name'], 'safe'],
            [['name', 'street', 'invoice_email', 'contact_email'], 'string', 'max' => 255],
            [['postcode', 'lang'], 'string', 'max' => 10],
            [['city'], 'string', 'max' => 20],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Nazwa',
            'street' => 'Ulica',
            'postcode' => 'Kod pocztowy',
            'city' => 'Miasto',
            'phone' => 'Telefon',
            'tax_num' => 'NIP',
            'invoice_email' => 'Email do faktury',
            'contact_email' => 'Email kontaktowy',
            'lang' => 'Język',
            'url' => 'Url',
            'added_by' => 'Dodane przez',
            'added_by_name' => 'Dodane przez usera',
            'added_on' => 'Dodane dnia',
        ];
    }

    public function attributes()
    {
        return array_merge(parent::attributes(), ['added_by_name']);
    }


    /**
     * @return \yii\db\ActiveRelation
     */
    public function getUserName()
    {
        return $this->hasOne(Users::className(), ['id' => 'added_by']);
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {

            $this->added_by = \Yii::$app->user->identity->id;

            return true;

        } else {

            return false;

        }
    }
}

?>
