<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "campaigns".
 *
 * @property integer $id
 * @property string $name
 * @property string $std_tracking_params
 * @property string $mobile_tracking_params
 * @property integer $cpc
 * @property integer $budget
 * @property string $traffic_provider
 * @property integer $segmentation_id
 * @property string $start_date
 * @property string $end_date
 * @property integer $added_by
 * @property string $added_on
 */
class Campaigns extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'campaigns';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['cpc', 'budget', 'segmentation_id', 'added_by'], 'integer'],
            [['start_date', 'end_date', 'added_on'], 'safe'],
            [['name', 'std_tracking_params', 'mobile_tracking_params'], 'string', 'max' => 255],
            [['traffic_provider'], 'string', 'max' => 10],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Nazwa',
            'std_tracking_params' => 'Parametry Std Tracking',
            'mobile_tracking_params' => 'Parametry Mobile Tracking',
            'cpc' => 'Cost per click',
            'budget' => 'Budżet',
            'traffic_provider' => 'Traffic Provider',
            'segmentation_id' => 'Segmentation ID',
            'start_date' => 'Data startu',
            'end_date' => 'Data końcowa',
            'added_by' => 'Dodane przez',
            'added_on' => 'Dodane dnia',
        ];
    }
}
