<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Campaigns;

/**
 * CampaignsSearch represents the model behind the search form about `backend\models\Campaigns`.
 */
class CampaignsSearch extends Campaigns
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'cpc', 'budget', 'segmentation_id', 'added_by'], 'integer'],
            [['name', 'std_tracking_params', 'mobile_tracking_params', 'traffic_provider', 'start_date', 'end_date', 'added_on'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Campaigns::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'cpc' => $this->cpc,
            'budget' => $this->budget,
            'segmentation_id' => $this->segmentation_id,
            'start_date' => $this->start_date,
            'end_date' => $this->end_date,
            'added_by' => $this->added_by,
            'added_on' => $this->added_on,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'std_tracking_params', $this->std_tracking_params])
            ->andFilterWhere(['like', 'mobile_tracking_params', $this->mobile_tracking_params])
            ->andFilterWhere(['like', 'traffic_provider', $this->traffic_provider]);

        return $dataProvider;
    }
}
