<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Partners;

/**
 * PartnersSearch represents the model behind the search form about `backend\models\Partners`.
 */
class PartnersSearch extends Partners
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tax_num', 'added_by'], 'integer'],
            [['name', 'phone', 'lang', 'url'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Partners::find();
        $query->select([
                'partners.id',
                'name',
                'street',
                'postcode',
                'city',
                'partners.phone',
                'tax_num',
                'invoice_email',
                'contact_email',
                'lang',
                'url',
                'user.id as added_by',
                'user.username as added_by_name',
                'added_on',
            ]
        )
            ->from('partners')
            ->join('LEFT OUTER JOIN', 'user',
                'user.id = partners.added_by') ;

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            //$query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'tax_num', $this->tax_num])
            ->andFilterWhere(['like', 'partners.phone', $this->phone])
            ->andFilterWhere(['like', 'invoice_email', $this->invoice_email])
            ->andFilterWhere(['like', 'contact_email', $this->contact_email])
            ->andFilterWhere(['like', 'lang', $this->lang])
            ->andFilterWhere(['like', 'url', $this->url]);

        return $dataProvider;
    }
}