<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "orders".
 *
 * @property integer $id
 * @property string $agreement_date
 * @property integer $monthly_budget
 * @property string $date_from
 * @property string $date_until
 * @property string $rep_firstname
 * @property string $rep_lastname
 * @property string $rep_position
 * @property integer $partner_id
 * @property integer $added_by
 * @property string $added_on
 */
class Orders extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'orders';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['agreement_date', 'date_from', 'date_until', 'added_on'], 'safe'],
            [['monthly_budget', 'partner_id', 'added_by'], 'integer'],
            [['rep_firstname', 'rep_lastname'], 'string', 'max' => 30],
            [['rep_position'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'agreement_date' => 'Data umowy',
            'monthly_budget' => 'Miesięczny budżet',
            'date_from' => 'Od daty',
            'date_until' => 'Do daty',
            'rep_firstname' => 'Imię przedst.',
            'rep_lastname' => 'Nazwisko przedstaw.',
            'rep_position' => 'Stanowisko przedstaw.',
            'partner_id' => 'Partner',
            'added_by' => 'Dodane przez',
            'added_on' => 'Dodane dnia',
        ];
    }
}
