<?php

namespace backend\models;

use Yii;
use yii\db\Query;
use yii\data\ActiveDataProvider;

/**
 * This is the model class for table "user".
 *
 * @property integer $id
 * @property integer $partner_id
 * @property string $username
 * @property string $firstname
 * @property string $lastname
 * @property string $auth_key
 * @property string $password_hash
 * @property string $password_reset_token
 * @property integer $role
 * @property string $email
 * @property string $phone
 * @property string $registration_date
 * @property string $last_visit
 * @property string $facebook_id
 * @property integer $status
 */
class Users extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                ['username', 'firstname', 'lastname', 'auth_key', 'password_hash', 'role', 'email', 'phone'],
                'required',
                'message'=>'Prosze uzupełnić pole: {attribute}.'
            ],
            [['partner_id', 'role', 'status'], 'integer'],
            [['registration_date', 'last_visit'], 'safe'],
            [['username', 'firstname', 'lastname', 'password_hash', 'password_reset_token', 'email', 'phone', 'facebook_id'], 'string', 'max' => 255],
            [['auth_key'], 'string', 'max' => 32],
            [['username'], 'unique'],
            [['password_reset_token'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'partner_id' => 'Partner',
            'partner_name' => 'Nazwa partnera',
            'username' => 'Login',
            'firstname' => 'Imię',
            'lastname' => 'Nazwisko',
            'auth_key' => 'Auth Key',
            'password_hash' => 'Hasło',
            'password_reset_token' => 'Token resetujący hasło',
            'role' => 'Rola',
            'email' => 'Email',
            'phone' => 'Telefon',
            'registration_date' => 'Data rejestracji',
            'last_visit' => 'Ostatnia wizyta',
            'facebook_id' => 'Facebook ID',
            'status' => 'Status',
        ];
    }

    public function attributes()
    {
        return array_merge(parent::attributes(), ['partner_name']);
    }

    public function getPartnersName()
    {
        return $this->hasOne(Partners::className(), ['id' => 'partner_id']);
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {

            $newUser             = Yii::$app->request->post('Users');
            !empty($newUser["partner_id"]) ? $this->partner_id = $newUser["partner_id"] : $this->partner_id;
            $this->password_hash =  Yii::$app->security->generatePasswordHash($this->password_hash);//password_hash($password, PASSWORD_DEFAULT);

            if ($this->isNewRecord)
            {
                $this->auth_key  = \Yii::$app->security->generateRandomString();
            }

            return true;

        } else {

            return false;

        }
    }
}
