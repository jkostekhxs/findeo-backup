<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "segmentations".
 *
 * @property integer $id
 * @property string $market
 * @property string $transaction_type
 * @property string $object_type
 * @property integer $location
 * @property integer $added_by
 * @property string $added_on
 */
class Segmentations extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'segmentations';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['location', 'added_by'], 'integer'],
            [['added_on'], 'safe'],
            [['market', 'transaction_type', 'object_type'], 'string', 'max' => 10],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'market' => 'Market',
            'transaction_type' => 'Transaction Type',
            'object_type' => 'Object Type',
            'location' => 'Location',
            'added_by' => 'Added By',
            'added_on' => 'Added On',
        ];
    }
}
