<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "prices".
 *
 * @property integer $id
 * @property string $type
 * @property string $product_name
 * @property string $price
 * @property integer $added_by
 * @property string $added_on
 */
class Prices extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'prices';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['price'], 'number'],
            [['added_by'], 'integer'],
            [['added_on'], 'safe'],
            [['type'], 'string', 'max' => 255],
            [['product_name'], 'string', 'max' => 30],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'type' => 'Typ',
            'product_name' => 'Nazwa produktu',
            'price' => 'Cena',
            'added_by' => 'Dodane przez',
            'added_on' => 'Dodane dnia',
        ];
    }
}
