<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "invoices".
 *
 * @property integer $id
 * @property string $name
 * @property integer $partner_id
 * @property string $doc_number
 * @property string $invoice_date
 * @property string $sell_date
 * @property string $payment_deadline
 * @property string $lang
 * @property string $amount_net
 * @property string $amount_gross
 * @property string $amount_final
 * @property double $vat
 * @property integer $added_by
 * @property string $added_on
 */
class Invoices extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'invoices';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['partner_id', 'added_by'], 'integer'],
            [['invoice_date', 'sell_date', 'payment_deadline', 'added_on'], 'safe'],
            [['amount_net', 'amount_gross', 'amount_final', 'vat'], 'number'],
            [['name'], 'string', 'max' => 255],
            [['doc_number', 'lang'], 'string', 'max' => 30],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Nazwa',
            'partner_id' => 'Partner',
            'doc_number' => 'Numer dokumentu',
            'invoice_date' => 'Data wystawienia',
            'sell_date' => 'Data sprzedaży',
            'payment_deadline' => 'Płatne do',
            'lang' => 'Język',
            'amount_net' => 'Kwota netto',
            'amount_gross' => 'Kwota brutto',
            'amount_final' => 'Kwota łącznie',
            'vat' => '% Vat',
            'added_by' => 'Dodane przez',
            'added_on' => 'Dodane dnia',
        ];
    }
}
