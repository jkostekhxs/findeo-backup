<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Invoices;

/**
 * InvoicesSearch represents the model behind the search form about `backend\models\Invoices`.
 */
class InvoicesSearch extends Invoices
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'partner_id', 'added_by'], 'integer'],
            [['name', 'doc_number', 'invoice_date', 'sell_date', 'payment_deadline', 'lang', 'added_on'], 'safe'],
            [['amount_net', 'amount_gross', 'amount_final', 'vat'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Invoices::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'partner_id' => $this->partner_id,
            'invoice_date' => $this->invoice_date,
            'sell_date' => $this->sell_date,
            'payment_deadline' => $this->payment_deadline,
            'amount_net' => $this->amount_net,
            'amount_gross' => $this->amount_gross,
            'amount_final' => $this->amount_final,
            'vat' => $this->vat,
            'added_by' => $this->added_by,
            'added_on' => $this->added_on,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'doc_number', $this->doc_number])
            ->andFilterWhere(['like', 'lang', $this->lang]);

        return $dataProvider;
    }
}
