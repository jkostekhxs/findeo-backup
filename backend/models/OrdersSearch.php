<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Orders;

/**
 * OrdersSearch represents the model behind the search form about `backend\models\Orders`.
 */
class OrdersSearch extends Orders
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'monthly_budget', 'partner_id', 'added_by'], 'integer'],
            [['agreement_date', 'date_from', 'date_until', 'rep_firstname', 'rep_lastname', 'rep_position', 'added_on'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Orders::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'agreement_date' => $this->agreement_date,
            'monthly_budget' => $this->monthly_budget,
            'date_from' => $this->date_from,
            'date_until' => $this->date_until,
            'partner_id' => $this->partner_id,
            'added_by' => $this->added_by,
            'added_on' => $this->added_on,
        ]);

        $query->andFilterWhere(['like', 'rep_firstname', $this->rep_firstname])
            ->andFilterWhere(['like', 'rep_lastname', $this->rep_lastname])
            ->andFilterWhere(['like', 'rep_position', $this->rep_position]);

        return $dataProvider;
    }
}
