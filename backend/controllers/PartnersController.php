<?php
namespace backend\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LoginForm;

use backend\models\Partners;
use backend\models\PartnersSearch;


use yii\data\ActiveDataProvider;

/**
 * Users controller
 */
class PartnersController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'error'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['logout', 'index'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['create'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['update'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['view'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['delete'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['debug'],
                        'allow' => true,
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function actionIndex()
    {
        $model = new Partners();
        $searchModel = new PartnersSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'model' => $model,
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
        ]);
    }


    /**
     * Displays partner add page.
     *
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Partners();

        if ($model->load(Yii::$app->request->post())) {
            if ($model->validate()) {
                // form inputs are valid, do something here
                //return;
            }
            $model->save();
            return $this->redirect(\Yii::$app->urlManager->createUrl('partners/index'));
        }

        return $this->render('create', [
        'model' => $model,
        ]);
    }

    /**
     * Displays partner update page.
     *
     * @return mixed
     */
    public function actionUpdate()
    {
        $request = Yii::$app->request;
        $id = $request->get('id');
        $model = Partners::findOne($id);

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Displays partner view page.
     *
     * @return mixed
     */
    public function actionView()
    {
        $model = new Partners();

        return $this->render('view', [
            'model' => $model,
        ]);
    }

    /**
     * Displays partner delete page.
     *
     * @return mixed
     */
    public function actionDelete()
    {
        $request = Yii::$app->request;
        $id = $request->get('id');

        $model = Partners::findOne($id);

        if (!$model->delete())
            Yii::$app->session->setFlash('error', 'Usuwanie nie powiodło się');

        return $this->redirect(\Yii::$app->urlManager->createUrl('partners/index'));
    }

}
