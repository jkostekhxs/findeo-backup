<?php
namespace backend\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LoginForm;

use backend\models\Partners;
use backend\models\Users;
use backend\models\Orders;
use backend\models\Invoices;
use backend\models\Prices;
use backend\models\Campaigns;
use backend\models\Segmentations;


use yii\data\ArrayDataProvider;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'error'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['logout', 'index'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['gii'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['debug'],
                        'allow' => true,
                    ],

                    ['actions' => ['partners'],    'allow' => true,],
                    ['actions' => ['partner_add'],    'allow' => true,],

                    ['actions' => ['user'],    'allow' => true,],
                    ['actions' => ['users'],    'allow' => true,],
                    ['actions' => ['user_add'],    'allow' => true,],

                    ['actions' => ['order'],     'allow' => true,],
                    ['actions' => ['orders'],     'allow' => true,],

                    ['actions' => ['invoice'],  'allow' => true,],
                    ['actions' => ['invoices'],  'allow' => true,],
                    ['actions' => ['invoice_add'],  'allow' => true,],

                    ['actions' => ['price'],         'allow' => true,],
                    ['actions' => ['prices'],         'allow' => true,],
                    ['actions' => ['price_add'],         'allow' => true,],

                    ['actions' => ['campaign'],      'allow' => true,],
                    ['actions' => ['campaigns'],      'allow' => true,],
                    ['actions' => ['campaign_add'],      'allow' => true,],
                    
                    ['actions' => ['statistics'],    'allow' => true,],

                    ['actions' => ['traffic'],    'allow' => true,],
                    ['actions' => ['analysis'],    'allow' => true,],
                    ['actions' => ['settlements'],     'allow' => true,],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays debug page.
     *
     * @return mixed
     */
    public function actionDebug()
    {
        return $this->render('debug');
    }

    /**
     * Migrates up.
     *
     * @return mixed
     */
    public function actionMigrateup()
    {

        $oldApp = \Yii::$app;
        new \yii\console\Application([
            'id'            => 'Command runner',
            'basePath'      => '@app',
            'components'    => [
                'db' => $oldApp->db,
            ],
        ]);
        \Yii::$app->runAction('migrate/up', ['migrationPath' => '@console/migrations/', 'interactive' => false]);
        \Yii::$app = $oldApp;
    }

    /**
     * Displays single user page.
     *
     * @return mixed
     */
    public function actionUser()
    {

        return $this->render('user');
    }

    /**
     * Displays users page.
     *
     * @return mixed
     */
    public function actionUsers()
    {
        $model = new Users();

        if ($model->load(Yii::$app->request->post())) {
            if ($model->validate()) {
                // form inputs are valid, do something here
                return;
            }
        }

        return $this->render('users', [
            'model' => $model,
        ]);
    }

    /**
     * Displays user add page.
     *
     * @return mixed
     */
    public function actionUser_add()
    {
        $model = new Users();

        if ($model->load(Yii::$app->request->post())) {
            if ($model->validate()) {
                // form inputs are valid, do something here
                return;
            }
        }

        return $this->render('user_add', [
            'model' => $model,
        ]);
    }

    /**
     * Displays campaigns page.
     *
     * @return mixed
     */
    public function actionCampaigns()
    {
        $model = new Campaigns();

        if ($model->load(Yii::$app->request->post())) {
            if ($model->validate()) {
                // form inputs are valid, do something here
                return;
            }
        }

        return $this->render('campaigns', [
            'model' => $model,
        ]);
    }

    /**
     * Displays campaign add page.
     *
     * @return mixed
     */
    public function actionCampaign_add()
    {
        $model = new Campaigns();

        if ($model->load(Yii::$app->request->post())) {
            if ($model->validate()) {
                // form inputs are valid, do something here
                return;
            }
        }

        return $this->render('campaign_add', [
            'model' => $model,
        ]);
    }

    /**
     * Displays segmentations page.
     *
     * @return mixed
     */
    public function actionSegmentations()
    {
        $model = new Segmentations();

        if ($model->load(Yii::$app->request->post())) {
            if ($model->validate()) {
                // form inputs are valid, do something here
                return;
            }
        }

        return $this->render('segmentations', [
            'model' => $model,
        ]);
    }

    /**
     * Displays prices page.
     *
     * @return mixed
     */
    public function actionPrices()
    {
        $model = new Prices();

        if ($model->load(Yii::$app->request->post())) {
            if ($model->validate()) {
                // form inputs are valid, do something here
                return;
            }
        }

        return $this->render('prices', [
            'model' => $model,
        ]);
    }


    /**
     * Displays price add page.
     *
     * @return mixed
     */
    public function actionPrice_add()
    {
        $model = new Prices();

        if ($model->load(Yii::$app->request->post())) {
            if ($model->validate()) {
                // form inputs are valid, do something here
                return;
            }
        }

        return $this->render('price_add', [
            'model' => $model,
        ]);
    }

    /**
     * Displays invoices page.
     *
     * @return mixed
     */
    public function actionInvoices()
    {
        $model = new Invoices();

        if ($model->load(Yii::$app->request->post())) {
            if ($model->validate()) {
                // form inputs are valid, do something here
                return;
            }
        }

        return $this->render('invoices', [
            'model' => $model,
        ]);
    }


    /**
     * Displays invoice add page.
     *
     * @return mixed
     */
    public function actionInvoice_add()
    {
        $model = new Invoices();

        if ($model->load(Yii::$app->request->post())) {
            if ($model->validate()) {
                // form inputs are valid, do something here
                return;
            }
        }

        return $this->render('invoice_add', [
            'model' => $model,
        ]);
    }

    /**
     * Displays partners page.
     *
     * @return mixed
     */
    public function actionPartners()
    {
        $model = new Partners();

        if ($model->load(Yii::$app->request->post())) {
            if ($model->validate()) {
                // form inputs are valid, do something here
                return;
            }
        }

        return $this->render('partners', [
            'model' => $model,
        ]);
    }


    /**
     * Displays partner add page.
     *
     * @return mixed
     */
    public function actionPartner_add()
    {
        $model = new Partners();

        if ($model->load(Yii::$app->request->post())) {
            if ($model->validate()) {
                // form inputs are valid, do something here
                return;
            }
        }

        return $this->render('partner_add', [
            'model' => $model,
        ]);
    }

    /**
     * Displays orders page.
     *
     * @return mixed
     */
    public function actionOrders()
    {
        $model = new Orders();

        if ($model->load(Yii::$app->request->post())) {
            if ($model->validate()) {
                // form inputs are valid, do something here
                return;
            }
        }

        return $this->render('orders', [
            'model' => $model,
        ]);
    }

    /**
     * Displays statistics page.
     *
     * @return mixed
     */
    public function actionStatistics()
    {
        return $this->render('statistics');
    }

    /**
     * Displays traffic page.
     *
     * @return mixed
     */
    public function actionTraffic()
    {
        return $this->render('traffic');
    }

    /**
     * Displays analysis page.
     *
     * @return mixed
     */
    public function actionAnalysis()
    {
        return $this->render('analysis');
    }

    /**
     * Displays settlements page.
     *
     * @return mixed
     */
    public function actionSettlements()
    {
        return $this->render('settlements');
    }

}
