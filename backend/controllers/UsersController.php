<?php
namespace backend\controllers;

use Yii;
use yii\base\Exception;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use backend\models\Users;
use backend\models\UsersSearch;


use yii\data\ActiveDataProvider;

/**
 * Users controller
 */
class UsersController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'error'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['logout', 'index'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['create'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['update'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['view'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['delete'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['debug'],
                        'allow' => true,
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function actionIndex()
    {
        $model = new Users();
        $searchModel = new UsersSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);


        return $this->render('index', [
            'model' => $model,
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,//->search(Yii::$app->request->queryParams),
        ]);
    }

    /**
     * Displays user add page.
     *
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Users();

        if ($model->load(Yii::$app->request->post())) {

            $model->beforeSave(1);

            if ($model->validate()) {
                // form inputs are valid, do something here

                $saved = $model->save();

                if(!$saved){
                    //echo Yii::$app->session->getFlash('error');
                    //Yii::$app->session->setFlash('error', 'Błąd podczas zapisu');
                }else{
                    return $this->redirect(\Yii::$app->urlManager->createUrl('users/index'));
                }

                //print_r($model->getErrors()); die();

                //return;
            }
        }
        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Displays user update page.
     *
     * @return mixed
     */
    public function actionUpdate()
    {
        $request = Yii::$app->request;
        $id = $request->get('id');
        $model = Users::findOne($id);

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Displays user view page.
     *
     * @return mixed
     */
    public function actionView()
    {
        $model = new Users();

        return $this->render('view', [
            'model' => $model,
        ]);
    }

    /**
     * Displays user delete page.
     *
     * @return mixed
     */
    public function actionDelete()
    {
        $request = Yii::$app->request;
        $id = $request->get('id');
        $model = Users::findOne($id);

        if (!$model->delete())
            Yii::$app->session->setFlash('error', 'Usuwanie nie powiodło się');

        return $this->redirect(\Yii::$app->urlManager->createUrl('users/index'));
    }

}
