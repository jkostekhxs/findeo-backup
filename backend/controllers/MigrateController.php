<?php
namespace backend\controllers;

use backend\components\ConsoleAppHelper;
use Yii;
use yii\web\Controller;

/**
 * Site controller
 */
class MigrateController extends Controller
{
    public function actionUp() {
        set_time_limit(0);
        $res = ConsoleAppHelper::runAction('migrate/up', ['migrationPath' => '@console/migrations/', 'interactive' => false]);
        var_dump($res); exit;
    }

    public function actionDown() {
        set_time_limit(0);
        $res = ConsoleAppHelper::runAction('migrate/down', ['migrationPath' => '@console/migrations/', 'interactive' => false]);
        var_dump($res); exit;
    }
}

?>