<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\Partners */

$this->title = 'Dodaj partnera';
$this->params['breadcrumbs'][] = ['label' => 'Partnerzy', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="partners-create">
    <div class="box box-primary">
        <div class="row">
            <div class="col-xs-12">
                <div class="inner-content">
                    <h1><?= Html::encode($this->title) ?></h1>

                    <?= $this->render('_form', [
                        'model' => $model,
                    ]) ?>
                </div>
            </div>
        </div>
    </div>
</div>
