<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\PartnersSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Partnerzy';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="partners-index">
    <div class="box box-primary">
        <div class="row">
            <div class="col-xs-12">
                <div class="inner-content table-responsive">
                    <h1><?= Html::encode($this->title) ?></h1>
                    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

                    <p>
                        <?= Html::a('Dodaj partnera', ['create'], ['class' => 'btn btn-success']) ?>
                    </p>
                    <?= GridView::widget([
                        'dataProvider' => $dataProvider,
                        'filterModel' => $searchModel,
                        'columns' => [
                            ['class' => 'yii\grid\SerialColumn'],

                            //'id',
                            //'name',
                            [
                                'attribute' => 'name',
                                'format' => 'text',
                                'label' => 'Nazwa',
                                'contentOptions' => ['class' => 'text-bold'],
                            ],
                            'street',
                            'postcode',
                            'city',
                            'phone',
                            'tax_num',
                            'invoice_email:email',
                            'contact_email:email',
                            'lang',
                            'url:url',
                            //'added_by',
                            [
                                'attribute' => 'added_by',
                                'label' => 'Dodane przez',
                                'value' => 'userName.username',
                            ],
                            'added_on',
                            [
                                'class' => 'yii\grid\ActionColumn',
                                'template' => '{view} {update} {delete}',
                                'buttons' => [
                                    'delete' => function($url, $model){
                                        return Html::a('<span class="glyphicon glyphicon-trash"></span>', ['delete', 'id' => $model->id], [
                                            'class' => '',
                                            'data' => [
                                                'confirm' => 'Jesteś pewien, że chcesz na trwałe usunąć tego partnera???',
                                                'method' => 'post',
                                            ],
                                        ]);
                                    }
                                ]
                            ],
                        ],
                        'emptyText' => 'Nie znaleziono wyników!',
                        'summary' => "Pozycje {begin} - {end}. Pozycji: {count}/{totalCount}. Strona: {page} z {pageCount}",
                    ]);
                    ?></div>
                </div>
            </div>
        </div>
    </div>
</div>
