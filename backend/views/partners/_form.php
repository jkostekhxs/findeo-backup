<?php

use yii\helpers\Html;
//use yii\widgets\ActiveForm;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\Partners */
/* @var $form yii\widgets\ActiveForm */

$template = [ 'template' => '{label} <div class="row"><div class="col-sm-4">{input}{error}{hint}</div></div>' ];
$template2 = [ 'template' => '<div class="row"><div class="col-sm-4"><div class="input-group"><span class="input-group-addon">@</span>{input}{error}{hint}</div></div></div>' ];

$wrapper1 = [
    'horizontalCssClasses' => [
        'wrapper' => 'col-md-2 col-sm-6 col-xs-12',
    ]
];
$wrapper2 = [
    'horizontalCssClasses' => [
        'wrapper' => 'col-md-3 col-sm-6 col-xs-12',
    ]
];

$layout = [
    'layout' => 'horizontal',
    'fieldConfig' => [
        'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
        'horizontalCssClasses' => [
            'label' => 'col-md-4 col-xs-12',
            'offset' => 'col-md-offset-4 col-md-offset-0',
            'wrapper' => 'col-md-3 col-sm-6 col-xs-12',
            'error' => '',
            'hint' => '',
        ],
    ],
];
?>

<!--<button data-toggle="collapse" data-target="#form" class="btn btn-success">Pokaż formularz</button>-->
<div class="gap"></div>
<div id="form" class="partners-form collapse in">

        <?php $form = ActiveForm::begin($layout); ?>

        <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'street')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'city')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'postcode')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'phone')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'tax_num')->textInput() ?>

        <?= $form->field($model, 'invoice_email')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'contact_email')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'lang')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'url')->textInput(['maxlength' => true]) ?>

        <?php /** $form->field($model, 'added_by')->textInput() ?>

        <?= $form->field($model, 'added_on')->textInput() **/ ?>

        <div class="row">
            <div class="col-xs-12 col-md-4 col-md-offset-4">
                <?= Html::submitButton($model->isNewRecord ? 'Dodaj' : 'Zmień', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-info']) ?>
            </div>
        </div>

        <?php ActiveForm::end(); ?>
    
</div>
<div class="gap"></div>
