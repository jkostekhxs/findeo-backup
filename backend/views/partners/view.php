<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use backend\models\Partners;

/* @var $this yii\web\View */
/* @var $model backend\models\Partners */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Partnerzy', 'url' => ['index']];

$this->title = 'Partner';
$this->params['breadcrumbs'][] = $this->title;
$model = new Partners();

?>
<div class="partners-view">
    <div class="row">
        <div class="col-xs-12">
            <div class="inner-content">
                <h1><?= Html::encode($this->title) ?></h1>

                <p>
                    <?= Html::a('Zmień', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
                    <?= Html::a('Usuń', ['delete', 'id' => $model->id], [
                        'class' => 'btn btn-danger',
                        'data' => [
                            'confirm' => 'Czy na pewno chcesz usunąć tego partnera?',
                            'method' => 'post',
                        ],
                    ]) ?>
                </p>

                <?= DetailView::widget([
                    'model' => $model,
                    'attributes' => [
                        'id',
                        'name',
                        'street',
                        'postcode',
                        'city',
                        'phone',
                        'tax_num',
                        'last_visit',
                        'invoice_email:email',
                        'contact_email:email',
                        'lang',
                        'url:url',
                        'added_by',
                        'added_on',
                    ],
                ]) ?>
            </div>
        </div>
    </div>
</div>
