<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Partners */

$this->title = 'Zmień: #' . $model->id . ': ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Partnerzy', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Uaktualnij';

?>
<div class="partners-update">
    <div class="box box-primary">
        <div class="row">
            <div class="col-xs-12">
                <div class="inner-content">
                    <h1><?= Html::encode($this->title) ?></h1>

                    <?= $this->render('_form', [
                        'model' => $model,
                    ]) ?>
                </div>
            </div>
        </div>
    </div>
</div>
