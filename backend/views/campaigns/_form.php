<?php

use yii\helpers\Html;
//use yii\widgets\ActiveForm;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\Campaigns */
/* @var $form yii\widgets\ActiveForm */

$layout = [
    'layout' => 'horizontal',
    'fieldConfig' => [
        'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
        'horizontalCssClasses' => [
            'label' => 'col-md-4 col-xs-12',
            'offset' => 'col-md-offset-4 col-md-offset-0',
            'wrapper' => 'col-md-3 col-sm-6 col-xs-12',
            'error' => '',
            'hint' => '',
        ],
    ],
];

?>

<!--<button data-toggle="collapse" data-target="#form" class="btn btn-success">Pokaż formularz</button>-->
<div class="gap"></div>
<div id="form" class="campaigns-form collapse in">

    <?php $form = ActiveForm::begin($layout); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'std_tracking_params')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'mobile_tracking_params')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'cpc')->textInput() ?>

    <?= $form->field($model, 'budget')->textInput() ?>

    <?= $form->field($model, 'traffic_provider')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'segmentation_id')->textInput() ?>

    <?= $form->field($model, 'start_date')->textInput() ?>

    <?= $form->field($model, 'end_date')->textInput() ?>

    <?php /** $form->field($model, 'added_by')->textInput() ?>

    <?= $form->field($model, 'added_on')->textInput() */ ?>

    <div class="row">
        <div class="col-xs-12 col-md-4 col-md-offset-4">
            <?= Html::submitButton($model->isNewRecord ? 'Dodaj' : 'Zmień', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-info']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<div class="gap"></div>