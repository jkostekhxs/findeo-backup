<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\PricesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Cennik';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="prices-index">
    <div class="box box-primary">
        <div class="row">
            <div class="col-xs-12">
                <div class="inner-content table-responsive">
                    <h1><?= Html::encode($this->title) ?></h1>
                    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

                    <p>
                        <?= Html::a('Dodaj', ['create'], ['class' => 'btn btn-success']) ?>
                    </p>
                    <?= GridView::widget([
                        'dataProvider' => $dataProvider,
                        'filterModel' => $searchModel,
                        'columns' => [
                            ['class' => 'yii\grid\SerialColumn'],

                            'id',
                            'type',
                            'product_name',
                            'price',
                            'added_by',
                            // 'added_on',

                            ['class' => 'yii\grid\ActionColumn'],
                        ],
                        'emptyText' => 'Nie znaleziono wyników!',
                        'summary' => "Pozycje {begin} - {end}. Pozycji: {count}/{totalCount}. Strona: {page} z {pageCount}",
                    ]); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

