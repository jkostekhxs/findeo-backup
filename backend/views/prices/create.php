<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\Prices */

$this->title = 'Stwórz cennik';
$this->params['breadcrumbs'][] = ['label' => 'Cennik', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="prices-create">
    <div class="box box-primary">
        <div class="row">
            <div class="col-xs-12">
                <div class="inner-content">
                    <h1><?= Html::encode($this->title) ?></h1>

                    <?= $this->render('_form', [
                        'model' => $model,
                    ]) ?>
                </div>
            </div>
        </div>
    </div>
</div>
