<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
//use yii\widgets\ActiveForm;
use yii\bootstrap\ActiveForm;

use yii\widgets\ActiveField;
use backend\models\Partners;


/* @var $this yii\web\View */
/* @var $model backend\models\Users */
/* @var $model backend\models\Partners */
/* @var $form yii\widgets\ActiveForm */

$modelPartners = new Partners();

$partnersData = Partners::find()->all();
$partnersList = ArrayHelper::map($partnersData, 'id', 'name');

$layout = [
    'layout' => 'horizontal',
    'fieldConfig' => [
        'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
        'horizontalCssClasses' => [
            'label' => 'col-md-4 col-xs-12',
            'offset' => 'col-md-offset-4 col-md-offset-0',
            'wrapper' => 'col-md-3 col-sm-6 col-xs-12',
            'error' => '',
            'hint' => '',
        ],
    ],
];

?>
<!--<button data-toggle="collapse" data-target="#form" class="btn btn-success">Pokaż formularz</button>-->
<div class="gap"></div>
<div id="form" class="users-form collapse in">

    <?php $form = ActiveForm::begin($layout); ?>

    <?= $form->errorSummary($model); ?>

    <?php //$form->field($model, 'partner_id')->textInput() ?>

    <?=
        $form
            ->field($model, 'partner_id')
            ->dropDownList(
                $partnersList,
                ['prompt' => '-- Wybierz partnera --']
            );
    ?>

    <?= $form->field($model, 'username')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'firstname')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'lastname')->textInput(['maxlength' => true]) ?>

    <?php // $form->field($model, 'auth_key')->textInput(['maxlength' => true]) ?>

    <?php // $form->field($model, 'password_hash')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'password_hash')->textInput(['maxlength' => true]) ?>

    <?php // $form->field($model, 'password_reset_token')->textInput(['maxlength' => true]) ?>

    <?php // $form->field($model, 'role')->textInput() ?>


    <?=
    $form
        ->field($model, 'role')
        ->dropDownList(
            ['10' => 'Admin', '20' => 'Klient'],
            ['prompt'=>' -- Wybierz rolę --']
        );
    ?>

    <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'phone')->textInput(['maxlength' => true]) ?>

    <?php // $form->field($model, 'registration_date')->textInput() ?>

    <?php // $form->field($model, 'last_visit')->textInput() ?>

    <?= $form->field($model, 'facebook_id')->textInput(['maxlength' => true]) ?>

    <?php // $form->field($model, 'status')->textInput() ?>

    <div class="row">
        <div class="col-xs-12 col-md-4 col-md-offset-4">
            <?= Html::submitButton($model->isNewRecord ? 'Dodaj' : 'Zmień', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-info']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<div class="gap"></div>
