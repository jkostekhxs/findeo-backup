<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\Users */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'User', 'url' => ['index']];

$this->title = 'Użytkownik';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="users-view">
    <div class="box box-primary">
        <div class="row">
            <div class="col-xs-12">
                <div class="inner-content">
                    <h1><?= Html::encode($this->title) ?></h1>

                    <p>
                        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
                        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
                            'class' => 'btn btn-danger',
                            'data' => [
                                'confirm' => 'Are you sure you want to delete this item?',
                                'method' => 'post',
                            ],
                        ]) ?>
                    </p>

                    <?= DetailView::widget([
                        'model' => $model,
                        'attributes' => [
                            'id',
                            'partner_id',
                            'username',
                            'firstname',
                            'lastname',
                            'auth_key',
                            'password_hash',
                            'password_reset_token',
                            'role',
                            'email:email',
                            'phone',
                            'registration_date',
                            'last_visit',
                            'facebook_id',
                            'status',
                        ],
                    ]) ?>
                </div>
            </div>
        </div>
    </div>
</div>
