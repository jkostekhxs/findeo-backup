<?php

use yii\helpers\Html;
use yii\grid\GridView;
use backend\models\Users;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\UsersSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Użytkownicy';
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="users-index">
    <div class="box box-primary">
        <div class="row">
            <div class="col-xs-12">
                <div class="inner-content table-responsive">
                    <h1><?= Html::encode($this->title) ?></h1>
                    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

                    <p>
                        <?= Html::a('Dodaj usera', ['create'], ['class' => 'btn btn-success']) ?>
                    </p>
                    <?= GridView::widget([
                        'dataProvider' => $dataProvider,
                        'filterModel' => $searchModel,
                        'columns' => [
                            [
                                'class' => 'yii\grid\SerialColumn'
                            ],
                            //'id',
                            'username',
                            'partner_name',
                            'firstname',
                            'lastname',
                             //'auth_key',
                             //'password_hash',
                             //'password_reset_token',
                             'role',
                             'email:email',
                             'phone',
                             'registration_date',
                             //'last_visit',
                             'facebook_id',
                            // 'status',
                            [
                                'header'=>'Akcje',
                                'class' => 'yii\grid\ActionColumn',
                                'template' => '{view} {update} {delete}',
                                'buttons' => [
                                    'delete' => function($url, $model){
                                        return Html::a('<span class="glyphicon glyphicon-trash"></span>', ['delete', 'id' => $model->id], [
                                            'class' => '',
                                            'data' => [
                                                'confirm' => 'Jesteś pewien, że chcesz na trwałe usunąć tego użytkownika???',
                                                'method' => 'post',
                                            ],
                                        ]);
                                    }
                                ]
                            ],
                        ],
                        'emptyText' => 'Nie znaleziono wyników!',
                        'summary' => "Pozycje {begin} - {end}. Pozycji: {count}/{totalCount}. Strona: {page} z {pageCount}",
                    ]); ?>
                </div>
            </div>
        </div>
    </div>
</div>
