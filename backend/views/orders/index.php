<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\OrdersSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Zamówienia';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="orders-index">
    <div class="box box-primary">
        <div class="row">
            <div class="col-xs-12">
                <div class="inner-content">
                    <h1><?= Html::encode($this->title) ?></h1>
                    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

                    <p>
                        <?= Html::a('Dodaj', ['create'], ['class' => 'btn btn-success']) ?>
                    </p>

                    <div class="box box-solid">
                        <div class="box-header with-border">
                            <h3 class="box-title">Zamówienia</h3>

                            <div class="box-tools">
                                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                                </button>
                            </div>
                        </div>
                        <div class="box-body no-padding">
                            <ul class="nav nav-pills nav-stacked">
                                <li><a href="#"><i class="fa fa-circle-o text-red"></i> Nowe</a></li>
                                <li><a href="#"><i class="fa fa-circle-o text-yellow"></i> W toku</a></li>
                                <li><a href="#"><i class="fa fa-circle-o text-green"></i> Zrealizowane</a></li>
                            </ul>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <div class="gap"></div>

                    <?= GridView::widget([
                        'dataProvider' => $dataProvider,
                        'filterModel' => $searchModel,
                        'columns' => [
                            ['class' => 'yii\grid\SerialColumn'],

                            'id',
                            'agreement_date',
                            'monthly_budget',
                            'date_from',
                            'date_until',
                            // 'rep_firstname',
                            // 'rep_lastname',
                            // 'rep_position',
                            // 'partner_id',
                            // 'added_by',
                            // 'added_on',

                            ['class' => 'yii\grid\ActionColumn'],
                        ],
                        'emptyText' => 'Nie znaleziono wyników!',
                        'summary' => "Pozycje {begin} - {end}. Pozycji: {count}/{totalCount}. Strona: {page} z {pageCount}",
                    ]); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

