<?php

use yii\helpers\Html;
//use yii\widgets\ActiveForm;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\Orders */
/* @var $form yii\widgets\ActiveForm */

$layout = [
    'layout' => 'horizontal',
    'fieldConfig' => [
        'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
        'horizontalCssClasses' => [
            'label' => 'col-md-4 col-xs-12',
            'offset' => 'col-md-offset-4 col-md-offset-0',
            'wrapper' => 'col-md-3 col-sm-6 col-xs-12',
            'error' => '',
            'hint' => '',
        ],
    ],
];

?>

<!--<button data-toggle="collapse" data-target="#form" class="btn btn-success">Pokaż formularz</button>-->
<div class="gap"></div>
<div id="form" class="orders-form collapse in">

    <?php $form = ActiveForm::begin($layout); ?>

    <?= $form->field($model, 'agreement_date')->textInput() ?>

    <?= $form->field($model, 'monthly_budget')->textInput() ?>

    <?= $form->field($model, 'date_from')->textInput() ?>

    <?= $form->field($model, 'date_until')->textInput() ?>

    <?= $form->field($model, 'rep_firstname')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'rep_lastname')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'rep_position')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'partner_id')->textInput() ?>

    <?php /** $form->field($model, 'added_by')->textInput() ?>

    <?= $form->field($model, 'added_on')->textInput() */ ?>

    <div class="row">
        <div class="col-xs-12 col-md-4 col-md-offset-4">
            <?= Html::submitButton($model->isNewRecord ? 'Dodaj' : 'Zmień', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-info']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<div class="gap"></div>
