<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\Orders */

$this->title = 'Stwórz zamówienie';
$this->params['breadcrumbs'][] = ['label' => 'Zamówienia', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="orders-create">
    <div class="box box-primary">
        <div class="row">
            <div class="col-xs-12">
                <div class="inner-content">
                    <h1><?= Html::encode($this->title) ?></h1>

                    <?= $this->render('_form', [
                        'model' => $model,
                    ]) ?>
                </div>
            </div>
        </div>
    </div>
</div>
