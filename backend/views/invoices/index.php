<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\InvoicesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Faktury';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="invoices-index">
    <div class="box box-primary">
        <div class="row">
            <div class="col-xs-12">
                <div class="inner-content table-responsive">
                    <h1><?= Html::encode($this->title) ?></h1>
                    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

                    <p>
                        <?= Html::a('Dodaj', ['create'], ['class' => 'btn btn-success']) ?>
                    </p>
                    <?= GridView::widget([
                        'dataProvider' => $dataProvider,
                        'filterModel' => $searchModel,
                        'columns' => [
                            ['class' => 'yii\grid\SerialColumn'],

                            'id',
                            'name',
                            'partner_id',
                            'doc_number',
                            'invoice_date',
                            // 'sell_date',
                            // 'payment_deadline',
                            // 'lang',
                            // 'amount_net',
                            // 'amount_gross',
                            // 'amount_final',
                            // 'vat',
                            // 'added_by',
                            // 'added_on',

                            ['class' => 'yii\grid\ActionColumn'],
                        ],
                        'emptyText' => 'Nie znaleziono wyników!',
                        'summary' => "Pozycje {begin} - {end}. Pozycji: {count}/{totalCount}. Strona: {page} z {pageCount}",
                    ]); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

