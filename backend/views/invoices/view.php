<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\Invoices */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Faktury', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="invoices-view">
    <div class="row">
        <div class="col-xs-12">
            <div class="inner-content">
                <h1><?= Html::encode($this->title) ?></h1>

                <p>
                    <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
                    <?= Html::a('Delete', ['delete', 'id' => $model->id], [
                        'class' => 'btn btn-danger',
                        'data' => [
                            'confirm' => 'Are you sure you want to delete this item?',
                            'method' => 'post',
                        ],
                    ]) ?>
                </p>

                <?= DetailView::widget([
                    'model' => $model,
                    'attributes' => [
                        'id',
                        'name',
                        'partner_id',
                        'doc_number',
                        'invoice_date',
                        'sell_date',
                        'payment_deadline',
                        'lang',
                        'amount_net',
                        'amount_gross',
                        'amount_final',
                        'vat',
                        'added_by',
                        'added_on',
                    ],
                ]) ?>
            </div>
        </div>
    </div>
</div>
