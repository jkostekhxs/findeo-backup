<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\InvoicesSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="invoices-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'name') ?>

    <?= $form->field($model, 'partner_id') ?>

    <?= $form->field($model, 'doc_number') ?>

    <?= $form->field($model, 'invoice_date') ?>

    <?php // echo $form->field($model, 'sell_date') ?>

    <?php // echo $form->field($model, 'payment_deadline') ?>

    <?php // echo $form->field($model, 'lang') ?>

    <?php // echo $form->field($model, 'amount_net') ?>

    <?php // echo $form->field($model, 'amount_gross') ?>

    <?php // echo $form->field($model, 'amount_final') ?>

    <?php // echo $form->field($model, 'vat') ?>

    <?php // echo $form->field($model, 'added_by') ?>

    <?php // echo $form->field($model, 'added_on') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
