<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\Users */
/* @var $form ActiveForm */
?>
<div class="users">

    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Użytkownicy</h3>

            <div class="box-tools pull-right">
                <div class="has-feedback">
                    <input type="text" class="form-control input-sm" placeholder="Szukaj usera">
                    <span class="glyphicon glyphicon-search form-control-feedback"></span>
                </div>
            </div>
            <!-- /.box-tools -->
        </div>
        <!-- /.box-header -->
        <div class="box-body no-padding">
            <div class="mailbox-controls">
                <!-- Check all button -->
                <button type="button" class="btn btn-default btn-sm checkbox-toggle"><i class="fa fa-square-o"></i>
                </button>
                <div class="btn-group">
                    <button type="button" class="btn btn-default btn-sm"><i class="fa fa-trash-o"></i></button>
                    <button type="button" class="btn btn-default btn-sm"><i class="fa fa-reply"></i></button>
                    <button type="button" class="btn btn-default btn-sm"><i class="fa fa-share"></i></button>
                </div>
                <!-- /.btn-group -->
                <button type="button" class="btn btn-default btn-sm"><i class="fa fa-refresh"></i></button>
                <div class="pull-right">
                    1-50/200
                    <div class="btn-group">
                        <button type="button" class="btn btn-default btn-sm"><i class="fa fa-chevron-left"></i></button>
                        <button type="button" class="btn btn-default btn-sm"><i class="fa fa-chevron-right"></i></button>
                    </div>
                    <!-- /.btn-group -->
                </div>
                <!-- /.pull-right -->
            </div>
            <div class="table-responsive mailbox-messages">
                <table class="table table-hover table-striped">
                    <tbody>
                    <tr>
                        <td><div class="icheckbox_flat-blue" aria-checked="false" aria-disabled="false" style="position: relative;"><input type="checkbox" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; border: 0px; opacity: 0; background: rgb(255, 255, 255);"></ins></div></td>
                        <td class="mailbox-name">
                                <?php echo Html::a(
                                'Admin',
                                ['/site/user'],
                                ['data-method' => 'post', 'class' => '']
                            ) ?></td>
                        <td class="mailbox-subject"><b>Super User</b> - Testowy profil
                        </td>
                        <td class="mailbox-attachment"><i class="fa fa-paperclip"></i></td>
                        <td class="mailbox-date">15 days ago</td>
                    </tr>
                    <tr>
                        <td><div class="icheckbox_flat-blue" aria-checked="false" aria-disabled="false" style="position: relative;"><input type="checkbox" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; border: 0px; opacity: 0; background: rgb(255, 255, 255);"></ins></div></td>
                        <td class="mailbox-name"><a href="read-mail.html">Jan Kowalski</a></td>
                        <td class="mailbox-subject"><b>Zwykły użytkownik</b> - Trying to find a solution to this problem...
                        </td>
                        <td class="mailbox-attachment"><i class="fa fa-paperclip"></i></td>
                        <td class="mailbox-date">15 days ago</td>
                    </tr>
                    <tr>
                        <td><div class="icheckbox_flat-blue" aria-checked="false" aria-disabled="false" style="position: relative;"><input type="checkbox" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; border: 0px; opacity: 0; background: rgb(255, 255, 255);"></ins></div></td>
                        <td class="mailbox-name"><a href="read-mail.html">Jan Kowalski</a></td>
                        <td class="mailbox-subject"><b>Zwykły użytkownik</b> - Trying to find a solution to this problem...
                        </td>
                        <td class="mailbox-attachment"><i class="fa fa-paperclip"></i></td>
                        <td class="mailbox-date">15 days ago</td>
                    </tr>
                    <tr>
                        <td><div class="icheckbox_flat-blue" aria-checked="false" aria-disabled="false" style="position: relative;"><input type="checkbox" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; border: 0px; opacity: 0; background: rgb(255, 255, 255);"></ins></div></td>
                        <td class="mailbox-name"><a href="read-mail.html">Jan Kowalski</a></td>
                        <td class="mailbox-subject"><b>Zwykły użytkownik</b> - Trying to find a solution to this problem...
                        </td>
                        <td class="mailbox-attachment"><i class="fa fa-paperclip"></i></td>
                        <td class="mailbox-date">15 days ago</td>
                    </tr>
                    <tr>
                        <td><div class="icheckbox_flat-blue" aria-checked="false" aria-disabled="false" style="position: relative;"><input type="checkbox" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; border: 0px; opacity: 0; background: rgb(255, 255, 255);"></ins></div></td>
                        <td class="mailbox-name"><a href="read-mail.html">Jan Kowalski</a></td>
                        <td class="mailbox-subject"><b>Zwykły użytkownik</b> - Trying to find a solution to this problem...
                        </td>
                        <td class="mailbox-attachment"><i class="fa fa-paperclip"></i></td>
                        <td class="mailbox-date">15 days ago</td>
                    </tr>
                    <tr>
                        <td><div class="icheckbox_flat-blue" aria-checked="false" aria-disabled="false" style="position: relative;"><input type="checkbox" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; border: 0px; opacity: 0; background: rgb(255, 255, 255);"></ins></div></td>
                        <td class="mailbox-name"><a href="read-mail.html">Jan Kowalski</a></td>
                        <td class="mailbox-subject"><b>Zwykły użytkownik</b> - Trying to find a solution to this problem...
                        </td>
                        <td class="mailbox-attachment"><i class="fa fa-paperclip"></i></td>
                        <td class="mailbox-date">15 days ago</td>
                    </tr>
                    <tr>
                        <td><div class="icheckbox_flat-blue" aria-checked="false" aria-disabled="false" style="position: relative;"><input type="checkbox" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; border: 0px; opacity: 0; background: rgb(255, 255, 255);"></ins></div></td>
                        <td class="mailbox-name"><a href="read-mail.html">Jan Kowalski</a></td>
                        <td class="mailbox-subject"><b>Zwykły użytkownik</b> - Trying to find a solution to this problem...
                        </td>
                        <td class="mailbox-attachment"><i class="fa fa-paperclip"></i></td>
                        <td class="mailbox-date">15 days ago</td>
                    </tr>
                    </tbody>
                </table>
                <!-- /.table -->
            </div>
            <!-- /.mail-box-messages -->
        </div>
        <!-- /.box-body -->
        <div class="box-footer no-padding">
            <div class="mailbox-controls">
                <!-- Check all button -->
                <button type="button" class="btn btn-default btn-sm checkbox-toggle"><i class="fa fa-square-o"></i>
                </button>
                <div class="btn-group">
                    <button type="button" class="btn btn-default btn-sm"><i class="fa fa-trash-o"></i></button>
                    <button type="button" class="btn btn-default btn-sm"><i class="fa fa-reply"></i></button>
                    <button type="button" class="btn btn-default btn-sm"><i class="fa fa-share"></i></button>
                </div>
                <!-- /.btn-group -->
                <button type="button" class="btn btn-default btn-sm"><i class="fa fa-refresh"></i></button>
                <div class="pull-right">
                    1-50/200
                    <div class="btn-group">
                        <button type="button" class="btn btn-default btn-sm"><i class="fa fa-chevron-left"></i></button>
                        <button type="button" class="btn btn-default btn-sm"><i class="fa fa-chevron-right"></i></button>
                    </div>
                    <!-- /.btn-group -->
                </div>
                <!-- /.pull-right -->
            </div>
        </div>
    </div>

</div><!-- partners -->
