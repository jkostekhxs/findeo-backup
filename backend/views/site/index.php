<?php

/* @var $this yii\web\View */

$this->title = 'Zaplecze';
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="row">
    <div cass="col-xs-12">
        <?php
        if( Yii::$app->user->identity->isAdmin(Yii::$app->user->identity->getId()) ){
            require_once('index_admin.php');
        }else{
            require_once('index_client.php');
        };

        ?>
    </div>
</div>
