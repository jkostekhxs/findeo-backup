<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'Debug';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-about">
    <h1><?= Html::encode($this->title) ?></h1>

    <p>This is the Debugging page. Not much here for now.</p>
</div>
