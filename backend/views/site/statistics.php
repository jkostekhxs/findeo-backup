<?php

use yii\helpers\Html;

/* @var $this yii\web\View */

$this->title = 'Statystyki';
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="statistics">

    <div class="box box-primary">
        <div class="box-header with-border">
            <i class="fa fa-bar-chart-o"></i>

            <h3 class="box-title">Statystyki</h3>

            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
            </div>
        </div>
        <div class="box-body">
            <div id="donut-chart" style="height: 300px; padding: 0px; position: relative;"><canvas class="flot-base" width="655" height="300" style="direction: ltr; position: absolute; left: 0px; top: 0px; width: 655px; height: 300px;"></canvas><canvas class="flot-overlay" width="655" height="300" style="direction: ltr; position: absolute; left: 0px; top: 0px; width: 655px; height: 300px;"></canvas><span class="pieLabel" id="pieLabel0" style="position: absolute; top: 71px; left: 386.5px;"><div style="font-size:13px; text-align:center; padding:2px; color: #fff; font-weight: 600;">Series2<br>30%</div></span><span class="pieLabel" id="pieLabel1" style="position: absolute; top: 211px; left: 364.5px;"><div style="font-size:13px; text-align:center; padding:2px; color: #fff; font-weight: 600;">Series3<br>20%</div></span><span class="pieLabel" id="pieLabel2" style="position: absolute; top: 130px; left: 205.5px;"><div style="font-size:13px; text-align:center; padding:2px; color: #fff; font-weight: 600;">Series4<br>50%</div></span></div>
        </div>
        <!-- /.box-body-->
    </div>

    <div class="box box-primary">
        <div class="box-header with-border">
            <i class="fa fa-bar-chart-o"></i>

            <h3 class="box-title">Wykres</h3>

            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
            </div>
        </div>
        <div class="box-body">
            <div id="line-chart" style="height: 300px; padding: 0px; position: relative;"><canvas class="flot-base" width="655" height="300" style="direction: ltr; position: absolute; left: 0px; top: 0px; width: 655px; height: 300px;"></canvas><div class="flot-text" style="position: absolute; top: 0px; left: 0px; bottom: 0px; right: 0px; font-size: smaller; color: rgb(84, 84, 84);"><div class="flot-x-axis flot-x1-axis xAxis x1Axis" style="position: absolute; top: 0px; left: 0px; bottom: 0px; right: 0px; display: block;"><div class="flot-tick-label tickLabel" style="position: absolute; max-width: 81px; top: 283px; left: 21px; text-align: center;">0</div><div class="flot-tick-label tickLabel" style="position: absolute; max-width: 81px; top: 283px; left: 113px; text-align: center;">2</div><div class="flot-tick-label tickLabel" style="position: absolute; max-width: 81px; top: 283px; left: 206px; text-align: center;">4</div><div class="flot-tick-label tickLabel" style="position: absolute; max-width: 81px; top: 283px; left: 298px; text-align: center;">6</div><div class="flot-tick-label tickLabel" style="position: absolute; max-width: 81px; top: 283px; left: 390px; text-align: center;">8</div><div class="flot-tick-label tickLabel" style="position: absolute; max-width: 81px; top: 283px; left: 479px; text-align: center;">10</div><div class="flot-tick-label tickLabel" style="position: absolute; max-width: 81px; top: 283px; left: 572px; text-align: center;">12</div></div><div class="flot-y-axis flot-y1-axis yAxis y1Axis" style="position: absolute; top: 0px; left: 0px; bottom: 0px; right: 0px; display: block;"><div class="flot-tick-label tickLabel" style="position: absolute; top: 270px; left: 1px; text-align: right;">-1.5</div><div class="flot-tick-label tickLabel" style="position: absolute; top: 225px; left: 1px; text-align: right;">-1.0</div><div class="flot-tick-label tickLabel" style="position: absolute; top: 180px; left: 1px; text-align: right;">-0.5</div><div class="flot-tick-label tickLabel" style="position: absolute; top: 135px; left: 4px; text-align: right;">0.0</div><div class="flot-tick-label tickLabel" style="position: absolute; top: 90px; left: 4px; text-align: right;">0.5</div><div class="flot-tick-label tickLabel" style="position: absolute; top: 45px; left: 4px; text-align: right;">1.0</div><div class="flot-tick-label tickLabel" style="position: absolute; top: 0px; left: 4px; text-align: right;">1.5</div></div></div><canvas class="flot-overlay" width="655" height="300" style="direction: ltr; position: absolute; left: 0px; top: 0px; width: 655px; height: 300px;"></canvas></div>
        </div>
        <!-- /.box-body-->
    </div>

</div><!-- statistics -->
