<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\Segmentations */
/* @var $form ActiveForm */
?>
<div class="segmentations">

    <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'location') ?>
        <?= $form->field($model, 'added_by') ?>
        <?= $form->field($model, 'added_on') ?>
        <?= $form->field($model, 'market') ?>
        <?= $form->field($model, 'transaction_type') ?>
        <?= $form->field($model, 'object_type') ?>
    
        <div class="form-group">
            <?= Html::submitButton('Submit', ['class' => 'btn btn-primary']) ?>
        </div>
    <?php ActiveForm::end(); ?>

</div><!-- segmentations -->
