<?php

namespace backend\assets;

use yii\web\AssetBundle;

/**
 * Main backend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/site.css',
        'css/calendar.css',
        'css/flot.css',
    ];
    public $js = [
        'js/knob/jquery.knob.min.js', //clicks
        'js/knob/jquery.knob-script.js',

        //'js/flot/jquery.flot.min.js', //Interactive area chart

        'js/flot/jquery.flot.js',
        //'js/flot/jquery.realtime-script.js',

        'js/chart/Chart.min.js',
        //'js/chart/jquery.chart-script.js',

        'js/sparkline/jquery.sparkline.min.js', //sparklines & sparkbars
        'js/sparkline/jquery.sparkline-script.js',

        'js/jvectormap/jquery-jvectormap-2.0.3.min.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
