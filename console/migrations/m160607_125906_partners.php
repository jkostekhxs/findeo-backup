<?php

use yii\db\Migration;

class m160607_125906_partners extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%partners}}', [

            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'street' => $this->string(),
            'postcode' => $this->string(10),
            'city' => $this->string(20),
            'tax_num' => $this->integer(),
            'phone' => $this->string(20),
            
            'invoice_email' => $this->string()->notNull(),
            'contact_email' => $this->string()->notNull(),
            'lang' => $this->string(10),
            'url' => $this->string(10),
            'added_by' => $this->smallInteger(10),
            'added_on' => $this->timestamp(),

        ], $tableOptions);
    }

    public function down()
    {
        $this->dropTable('{{%partners}}');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
