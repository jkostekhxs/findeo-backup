<?php

use yii\db\Migration;

class m160607_125833_segmentations extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%segmentations}}', [

            'id' => $this->primaryKey(),
            'market' => $this->string(10),
            'transaction_type' => $this->string(10),
            'object_type' => $this->string(10),
            'location' => $this->smallInteger(10),
            'added_by' => $this->smallInteger(10),
            'added_on' => $this->timestamp(),

        ], $tableOptions);
    }

    public function down()
    {
        $this->dropTable('{{%segmentations}}');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
