<?php

use yii\db\Migration;

class m160607_125758_campaigns extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%campaigns}}', [

            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'std_tracking_params' => $this->string(),
            'mobile_tracking_params' => $this->string(),
            'cpc' => $this->integer(),
            'budget' => $this->integer(),
            'traffic_provider' => $this->string(10),
            'segmentation_id' => $this->smallInteger(10),
            'start_date' => $this->date(),
            'end_date' => $this->date(),
            'added_by' => $this->smallInteger(10),
            'added_on' => $this->timestamp(),

        ], $tableOptions);
    }

    public function down()
    {
        $this->dropTable('{{%campaigns}}');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
