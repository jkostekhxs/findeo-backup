<?php

use yii\db\Migration;

class m160607_130914_orders extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%orders}}', [

            'id' => $this->primaryKey(),
            'agreement_date' => $this->date(),
            'monthly_budget' => $this->integer(),
            'date_from' => $this->date(),
            'date_until' => $this->date(),
            'rep_firstname' => $this->string(30),
            'rep_lastname' => $this->string(30),
            'rep_position' => $this->string(),
            'partner_id' => $this->smallInteger(10),

            'added_by' => $this->smallInteger(10),
            'added_on' => $this->timestamp(),

        ], $tableOptions);
    }

    public function down()
    {
        $this->dropTable('{{%orders}}');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
