<?php

use yii\db\Migration;

class m160607_130924_invoices extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%invoices}}', [

            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'partner_id' => $this->smallInteger(10),
            'doc_number' => $this->string(30),
            'invoice_date' => $this->date(),
            'sell_date' => $this->date(),
            'payment_deadline' => $this->date(),
            'lang' => $this->string(30),
            'amount_net' => $this->money(null, 2),
            'amount_gross' => $this->money(null, 2),
            'amount_final' => $this->money(null, 2),
            'vat' => $this->float(),

            'added_by' => $this->smallInteger(10),
            'added_on' => $this->timestamp(),

        ], $tableOptions);
    }

    public function down()
    {
        $this->dropTable('{{%invoices}}');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
