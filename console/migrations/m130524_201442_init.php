<?php

use yii\db\Migration;

class m130524_201442_init extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%user}}', [

            'id' => $this->primaryKey(),
            'partner_id' => $this->integer(),
            'username' => $this->string()->notNull()->unique(),
            'firstname' => $this->string()->notNull(),
            'lastname' => $this->string()->notNull(),

            'auth_key' => $this->string(32)->notNull(),
            'password_hash' => $this->string()->notNull(),
            'password_reset_token' => $this->string()->unique(),

            'role' => $this->smallInteger()->notNull(), //default user
            'email' => $this->string()->notNull(),
            'phone' => $this->string()->notNull(),
            'registration_date' => $this->date(),
            'last_visit' => $this->date(),
            'facebook_id' => $this->string(),
            'status' => $this->smallInteger()->notNull()->defaultValue(10),

        ], $tableOptions);


        $this->insert('user',array(

            'id' => 1,
            'partner_id' => 0,
            'username' => 'admin',
            'firstname' => 'Admin',
            'lastname' => 'Adminowy',

            'auth_key' => '33cR537z4eIx-gyO4J3Bgf9Gk2yVdanI',
            'password_hash' => Yii::$app->getSecurity()->generatePasswordHash('!@#$%^&'), //$2y$13$2pdDnZgcOjECksEBAI/pPOPLLgTodkD6kCl6yJnwpkADtiRPXsa22

            'role' => 10, //admin user
            'email' => '',
            'phone' => '',
            'facebook_id' => '',
        ));


        $this->insert('user',array(

            'partner_id' => 0,
            'username' => 'client',
            'firstname' => 'Klilentowy',
            'lastname' => 'Klient',

            'auth_key' => '33cR537z4eIx-gyO4J3Bgf9Gk2yVdanI',
            'password_hash' => Yii::$app->getSecurity()->generatePasswordHash('!@#$%^&'), //$2y$13$2pdDnZgcOjECksEBAI/pPOPLLgTodkD6kCl6yJnwpkADtiRPXsa22

            'role' => 20, //client user
            'email' => '',
            'phone' => '',
            'facebook_id' => '',
        ));
    }

    public function down()
    {
        $this->dropTable('{{%user}}');
    }
}
