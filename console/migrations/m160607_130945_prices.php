<?php

use yii\db\Migration;

class m160607_130945_prices extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%prices}}', [

            'id' => $this->primaryKey(),
            'type' => $this->string(),
            'product_name' => $this->string(30),
            'price' => $this->money(null, 2),

            'added_by' => $this->smallInteger(10),
            'added_on' => $this->timestamp(),

        ], $tableOptions);
    }

    public function down()
    {
        $this->dropTable('{{%prices}}');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
