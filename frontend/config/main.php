<?php
use \yii\web\Request;

$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

$baseUrlFront = str_replace('/backend/web', '/frontend/web', (new Request)->getBaseUrl());
$baseUrlBack  = str_replace('/frontend/web', '/backend/web', (new Request)->getBaseUrl());

return [
    'id' => 'app-frontend',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'controllerNamespace' => 'frontend\controllers',
    'components' => [
        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        /*
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
            ],
        ],
        */
        /*'request'=>[
            'baseUrl'=>'',
        ],
        'urlManager'=>[
            'scriptUrl'=>'/index.php',
        ],*/
        'urlManagerFrontEnd' => [
            'class'           => 'yii\web\urlManager',
            'baseUrl'         => $baseUrlFront,
            'enablePrettyUrl' => false,
            'showScriptName'  => false,
        ],
        'urlManagerBackEnd' => [
            'class'           => 'yii\web\urlManager',
            'baseUrl'         => $baseUrlBack,
            'enablePrettyUrl' => false,
            'showScriptName'  => false,
        ],
    ],
    'params' => $params,
];
