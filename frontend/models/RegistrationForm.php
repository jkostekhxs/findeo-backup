<?php

namespace frontend\models;

use Yii;

use yii\base\Model;
use common\models\User;

/**
 * This is the model class for table "user".
 *
 * @property integer $id
 * @property string $username
 * @property string $auth_key
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $nip
 * @property string $email
 * @property string $imie
 * @property string $nazwisko
 * @property string $telefon
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 */
class RegistrationForm extends \yii\db\ActiveRecord
{
    public $password;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['username', 'trim'],
            ['username', 'required'],
            ['username', 'unique', 'targetClass' => '\common\models\User', 'message' => 'This username has already been taken.'],
            ['username', 'string', 'min' => 2, 'max' => 255],

            ['email', 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'string', 'max' => 255],
            ['email', 'unique', 'targetClass' => '\common\models\User', 'message' => 'This email address has already been taken.'],

            ['password', 'required', 'message' => 'Please provide a password.'],
            ['password', 'string', 'min' => 6],

            ['nip', 'required', 'message' => 'Please provide a number.'],
            ['nip', 'unique', 'message' => 'This number has already been taken.'],
            ['nip', 'string', 'min' => 10],
            ['nip', 'string', 'max' => 10],

            ['telefon', 'string', 'max' => 255],

            [['nip', 'status', 'created_at', 'updated_at'], 'integer'],
            [['username', 'password_hash', 'password_reset_token', 'email', 'imie', 'nazwisko'], 'string', 'max' => 255],
            [['auth_key'], 'string', 'max' => 32],
            [['password_reset_token'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'username' => 'Username',
            'auth_key' => 'Auth Key',
            'password' => 'Password',
            'password_hash' => 'Password Hash',
            'password_reset_token' => 'Password Reset Token',
            'nip' => 'NIP',
            'email' => 'Email',
            'imie' => 'Imie',
            'nazwisko' => 'Nazwisko',
            'telefon' => 'Telefon',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * Registers user.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function register()
    {
        if (!$this->canRegister()) {
            return null;
        }

        $user = new User();
        $user->username = $this->username;
        $user->email = $this->email;
        $user->setPassword($this->password);
        $user->generateAuthKey();

        return $user->save() ? $user : null;
    }


    /**
     * Hashes passwd.
     *
     * @return bool
     */
    public function beforeSave($data=array())
    {
      //passwd hash
      if (!empty($this->password)) $this->password_hash = md5($this->password);
      return true;
    }

    /**
     * Check if user can be registered.
     *
     * @return bool
     */
    private function canRegister(){
      if(
        empty($this->nip) ||
        empty($this->username) ||
        empty($this->password) ||
        $this->getAttribute($this->nip) ||
        $this->getAttribute($this->username)
      ){
        return false;
      }else{
        return true;
      }
    }
}
