<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use borales\extensions\phoneInput\PhoneInput;

$this->title = 'Register';
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="site-register">
  <h1><?= Html::encode($this->title) ?></h1>
  <p>Please fill out the following fields to register:</p>
  <div class="row">
      <div class="col-lg-5">
        <?php $form = ActiveForm::begin(['id' => 'form-register']); ?>

            <?= $form->field($model, 'nip') ?>
            <?= $form->field($model, 'email') ?>
            <div class="row">
              <div class="col-lg-6">
                <?= $form->field($model, 'imie') ?>
              </div>
              <div class="col-lg-6">
                <?= $form->field($model, 'nazwisko') ?>
              </div>
            </div>
            <?= $form->field($model, 'telefon')->widget(PhoneInput::className(), [
                    'jsOptions' => [
                        'allowExtensions' => true,
                        'preferredCountries' => ['pl'],
                    ]
                ])->textInput(['type' => 'number']);
            ?>
            <hr/>
            <?= $form->field($model, 'username') ?>
            <?php /* $form->passwordField($model, 'password')*/ ?>

            <label class="control-label" for="registrationform-password">Password</label>
            <input type="text" id="registrationform-password" class="form-control" name="RegistrationForm[password]">
            <hr/>

            <div class="form-group">
                <?= Html::submitButton('Register', ['class' => 'btn btn-primary', 'name' => 'register-button']) ?>
            </div>
        <?php ActiveForm::end(); ?>
    </div><!-- register -->
  </div>
</div>
