# -*- coding: utf-8 -*-
import scrapy, json, sys
from crawler.items import MainItem
from mysqlConnector import db
from re import sub
from decimal import Decimal

# fro = sys.argv[-2]
# max_price = sys.argv[-1]

reload( sys )
sys.setdefaultencoding('utf8')

class OtodomSpider( scrapy.Spider ):
    name = "otodom"
    allowed_domains = ["http://otodom.pl/"]

    rent_interval = 100 # interval for offer to rent
    sell_interval = 1000 # interval for offer to sell

    start_urls = (
        "http://otodom.pl/wynajem/?search[filter_float_price%3Afrom]=0&search[filter_float_price%3Ato]=10&search[description]=1",
        )

    db = db() #class for connecting to DB
    
    # '''
    #     Generate categories url with filtred price
    # '''
    # def start_requests( self ):
    #     rent = range(fro, max_price, self.rent_interval)
    #     sell = range(fro, max_price * 100, self.sell_interval)
    #     urls = []
    #     for p in rent:
    #         fro = p
    #         if p != 0:
    #             p += 1
    #         to = fro + self.rent_interval
    #         urls.append("http://otodom.pl/wynajem/?search[filter_float_price%%3Afrom]=%s&search[filter_float_price%%3Ato]=%s&search[description]=1&nrAdsPerPage=72" % ( p, to ))

    #     for s in sell:
    #         fro = s
    #         if s != 0:
    #             s += 1
    #         to = fro + self.sell_interval
    #         urls.append("http://otodom.pl/sprzedaz/?search[filter_float_price%%3Afrom]=%s&search[filter_float_price%%3Ato]=%s&search[description]=1&nrAdsPerPage=72" % ( s, to ))

    #     for u in urls:
    #         yield scrapy.Request( u )

    '''
        Start parsing website
    '''
    def parse( self, response ):
        numOfPages = response.css('[class^="current"]::text').extract() # max number paginate
        links = response.css('a[class^="img-shadow"]::attr(href)').extract() # list of announcments
        if numOfPages: #Check if page is paginated
            nums = range( 1, int( numOfPages ) + 1 ) # form 1 to max mages + 1 ( for iterate, 0,1,2,..)
            if int( numOfPages[0] ) > 500: #Check number of paginate
                self.logger.info("Not crawled on %s.. Too many pages ( %s ).. Change Interwal!!" % (response.url, numOfPages))
                return
        elif not links: #Check if there are announcment
            self.logger.info("Not crawled on %s.. Don't find any announcment" % response.url)
            return
        else:
            nums = [1]
        ''' Start to itarate on page '''    
        for num in nums:
            item = MainItem()
            sufix = "&page=%s" % str(num)
            url = response.url + sufix
            if num == 1:
                url = response.url
            request = scrapy.Request( url=str( url ), callback=self.parseCat, dont_filter=True )
            request.meta['item'] = item
            yield request
    
    '''
        Parsing to get all Announcments Urls in one paginated page
    '''
    def parseCat( self, response ):
        links = response.css('a[class^="img-shadow"]::attr(href)').extract() # all announcmnets
        item = response.meta['item']
        for link in links:
            link = str(link)
            request2 = scrapy.Request( link, callback=self.parseOffer, dont_filter=True )
            request2.meta['item'] = item
            yield request2
    '''
        Start to proces one announcment
    '''
    def parseOffer( self, response ):
        ''' Check if announcment exist '''
        if self.db.checkIfExist( response.url ):
            return
        item = response.meta['item']
        items = self.handleImage( item, response )
        items = self.handleMainInfo( item, response )
        items = self.handleExtraInfo( item, response )
        items = self.handleMapInfo( item, response )
        if item:
            self.logger.info("****** Starting saving into db ******")
            return self.db.saveToDb( item, self.name, self.allowed_domains[0] )

    '''
        Getting list of images
    '''
    def handleImage( self, item, response ):
        picUrls =  response.xpath("//a[@class='gallery-box-thumb-item']/@href").extract()
        images = []
        for img in picUrls:
            images.append(str(img))
        item['Images'] = images

        return item

    '''
        Getting Main information
    '''
    def handleMainInfo( self, item, response ):
        item['Title'] = response.xpath('//header/h1/text()').extract()[0]
        item['MainUrl'] = response.url
        item['Type'] = response.xpath('//p[@class="address-links"]/a[@class="title"]/text()').extract()[0]
        info = response.xpath('//ul[@class="main-list"]/li/span/strong/text()').extract()
        flor = response.xpath('//ul[@class="main-list"]/li/text()').extract()[1]
        item['Price'] = Decimal( sub(r'[^\d.]', '', info[0]) )
        ''' Check if flor are passed '''
        if flor == "powierzchnia":
            item['Flor'] = info[1]
        else:
            item['Flor'] = "0"
        return item

    '''
        Getting additional information
    '''
    def handleExtraInfo( self, item, response ):
        content = response.xpath('//ul[@class="sub-list"]/li/text()').extract()
        subItems = {}
        if content:
           title = response.xpath('//ul[@class="sub-list"]/li/strong/text()').extract()
           subItems = json.dumps( dict( zip( title, content ) ), ensure_ascii=False )

        item['ExtraInfo'] = subItems

        return item

    '''
        Geting map info
    '''
    def handleMapInfo( self, item, response ):
        lon = str( response.xpath('//div[@id="adDetailInlineMap"]/@data-lon').extract()[0] )
        lat = str( response.xpath('//div[@id="adDetailInlineMap"]/@data-lat').extract()[0] )
        item['MapCoords'] = str( lon + ':' + lat )
        return item

    '''
        Check if Val exist
    '''
    def check( self, val ):
        if not val:
            val = 0
        return val