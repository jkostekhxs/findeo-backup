# -*- coding: utf-8 -*-
import MySQLdb, sys

reload( sys )
sys.setdefaultencoding('utf8')

class db():
    """ Class for connect and make request to db """
    def __init__(self):
        # self.db = MySQLdb.connect( 'localhost', 'root', 'BW20!cmd+d', 'adsourcing' )
        self.cn = MySQLdb.connect( 'localhost', 'root', '', 'bot_v2' )
        self.cursor = self.cn.cursor()
        self.fields = '(`title`, `price`, `typ`, `extra`, `map_coordinate`, `surface`, `provider_id`, `url`)' #fields in db ( need for query )
        ''' Setting charset '''
        self.cn.set_character_set('utf8')
        self.cursor.execute('SET NAMES utf8;')
        self.cursor.execute('SET CHARACTER SET utf8;')
        self.cursor.execute('SET character_set_connection=utf8;')

    '''
        Main function to save in DB 
            Item - element to be saved
            name - name of provider
            url - url of provider
    '''
    def saveToDb( self, item, name, url ):
        title = item['Title']
        typ = item['Type']
        flor = item['Flor']
        query = u"INSERT INTO `announcments` %s VALUES ( '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s')" % ( self.fields, title, item['Price'], typ, item['ExtraInfo'], item['MapCoords'], flor, self.getProviderId( name, url ), item['MainUrl'] )
        self.cursor.execute( query )

        if item['Images']:
            self.saveImages( item, self.cursor.lastrowid )

        self.cn.commit()
        print "#### SAVED ####"

    '''
        Find one element in db ( for provider_id)
    '''
    def findId( self, query ):
        self.cursor.execute( query )
        return self.cursor.fetchone()

    '''
        Getting provider id from DB, if not exist - create new
    '''
    def getProviderId( self, name, url ):
        query = "SELECT id FROM `providers` WHERE `name` LIKE '%s'" % name
        if not self.findId( query ):
            create = "INSERT INTO `providers` VALUES ( NULL, '%s', '%s' )" % ( name, url )
            self.cursor.execute( create )
            self.cn.commit()

        return self.findId( query )[0]

    '''
        Saving images to db
    '''
    def saveImages( self, item, annon_id ):
        for img in item['Images']:
            query = "INSERT INTO `images` VALUES ( NULL, '%s', '%s' )" % ( img, annon_id )
            self.cursor.execute( query )
            self.cn.commit()

    def checkIfExist( self, item_id ):
        query = "SELECT id FROM `announcments` WHERE `url` LIKE '%s'" % item_id
        exist = self.cursor.execute( query )
        if exist:
            return True
        return False