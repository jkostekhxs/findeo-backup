<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\InvoiceForm */
/* @var $form ActiveForm */

$this->title = 'Dane do faktury';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="invoice-register">
  <h1><?= Html::encode($this->title) ?></h1>
  <p>Please fill out the following fields to register:</p>
  <div class="row">
      <div class="col-lg-4 col-lg-offset-4 col-sm-12">
        <?php $form = ActiveForm::begin(['id' => 'form-invoice']); ?>

            <?= $form->field($model, 'nip') ?>
            <?= $form->field($model, 'nazwa_firmy') ?>
            <?= $form->field($model, 'ulica') ?>
            <?= $form->field($model, 'kod_pocztowy') ?>
            <?= $form->field($model, 'miejscowosc') ?>
            <?= $form->field($model, 'email') ?>

            <div class="form-group">
                <?= Html::submitButton('Send', ['class' => 'btn btn-primary']) ?>
            </div>
        <?php ActiveForm::end(); ?>
      </div><!-- invoice -->
  </div>
</div>
