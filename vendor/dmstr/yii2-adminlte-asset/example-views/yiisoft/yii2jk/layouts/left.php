<aside class="main-sidebar">

    <section class="sidebar">

        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="<?= $directoryAsset ?>/img/user2-160x160.jpg" class="img-circle" alt="User Image"/>
            </div>
            <div class="pull-left info">
                <p>AdSourcing</p>

                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>

        <!-- search form -->
        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search..."/>
              <span class="input-group-btn">
                <button type='submit' name='search' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
            </div>
        </form>
        <!-- /.search form -->

        <?php
        $nonadminMenu = dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu'],
                'items' => [
                    ['label' => 'Menu', 'options' => ['class' => 'header']],
                    ['label' => 'Pulpit', 'icon' => 'fa fa-desktop', 'url' => \Yii::$app->urlManagerBackEnd->baseUrl],
                    [
                        'label' => 'Ruch',
                        'icon' => 'fa fa-refresh',
                        'url' => ['/site/traffic']
                    ],
                    [
                        'label' => 'Kampanie',
                        'icon' => 'fa fa-th',
                        'url' => ['/site/campaigns']
                    ],
                    [
                        'label' => 'Analizy',
                        'icon' => 'fa fa-line-chart',
                        'url' => ['/site/analysis']
                    ],
                    ['label' => 'Rozliczenia', 'icon' => 'fa fa-cc-mastercard', 'url' => ['/site/settlements']],
                    [
                        'label' => 'Zarządzanie',
                        'icon' => 'fa fa-cogs',
                        'url' => '#',
                        'items' => [
                            ['label' => 'Raporty', 'icon' => 'fa fa-user', 'url' => ['/site/reports']],
                            ['label' => 'Konfiguracja', 'icon' => 'fa fa-cog', 'url' => ['/site/configuration']],
                            ['label' => 'Administracja', 'icon' => 'fa fa-sliders', 'url' => ['/site/administration']],
                        ],
                    ],
                ],
            ]
        );

        $adminMenu = dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu'],
                'items' => [
                    //jk
                    ['label' => 'Menu', 'options' => ['class' => 'header']],
                    ['label' => 'Pulpit', 'icon' => 'fa fa-desktop', 'url' => \Yii::$app->urlManagerBackEnd->baseUrl],
                    [
                        'label' => 'Partnerzy',
                        'icon' => 'fa fa-male',
                        'url' => '#',
                        'items' => [
                            ['label' => 'Lista Partnerów', 'icon' => 'fa fa-list', 'url' => ['/partners']],
                            ['label' => 'Dodaj partnera', 'icon' => 'fa fa-plus', 'url' => ['/partners/create']],
                        ],
                    ],
                    [
                        'label' => 'Użytkownicy',
                        'icon' => 'fa fa-users',
                        'url' => '#',
                        'items' => [
                            ['label' => 'Lista Userów', 'icon' => 'fa fa-list', 'url' => ['/users']],
                            ['label' => 'Dodaj usera', 'icon' => 'fa fa-plus', 'url' => ['/users/create']],
                        ],
                    ],
                    [
                        'label' => 'Zamówienia',
                        'icon' => 'fa fa-list-alt',
                        'url' => '#',
                        'items' => [
                            ['label' => 'Lista zamówień', 'icon' => 'fa fa-list', 'url' => ['/orders']],
                            ['label' => 'Złóż zamówienie', 'icon' => 'fa fa-plus', 'url' => ['/orders/create']],
                        ],
                    ],
                    [
                        'label' => 'Faktury',
                        'icon' => 'fa fa-book',
                        'url' => '#',
                        'items' => [
                            ['label' => 'Lista faktur', 'icon' => 'fa fa-list', 'url' => ['/invoices']],
                            ['label' => 'Wystaw fakturę', 'icon' => 'fa fa-plus', 'url' => ['/invoices/create']],
                        ],
                    ],
                    [
                        'label' => 'Cennik',
                        'icon' => 'fa fa-credit-card',
                        'url' => '#',
                        'items' => [
                            ['label' => 'Ceny usług', 'icon' => 'fa fa-list', 'url' => ['/prices']],
                            ['label' => 'Dodaj cennik', 'icon' => 'fa fa-plus', 'url' => ['/prices/create']],
                        ],
                    ],
                    [
                        'label' => 'Kampanie',
                        'icon' => 'fa fa-align-left',
                        'url' => '#',
                        'items' => [
                            ['label' => 'Lista kampanii', 'icon' => 'fa fa-list', 'url' => ['/campaigns']],
                            ['label' => 'Dodaj kampanię', 'icon' => 'fa fa-plus', 'url' => ['/campaigns/create']],
                        ],
                    ],
                    [
                        'label' => 'Statystyki',
                        'icon' => 'fa fa-bar-chart',
                        'url' => ['/site/statistics']
                    ],
                    [
                        'label' => 'Zarządzanie',
                        'icon' => 'fa fa-cogs',
                        'url' => '#',
                        'items' => [
                            ['label' => 'Raporty', 'icon' => 'fa fa-user', 'url' => ['/site/reports']],
                            ['label' => 'Konfiguracja', 'icon' => 'fa fa-cog', 'url' => ['/site/configuration']],
                            ['label' => 'Administracja', 'icon' => 'fa fa-sliders', 'url' => ['/site/administration']],
                        ],
                    ],
                    //old
                      [
                          'label' => 'Opcje programistyczne',
                          'options' => ['class' => 'header'],
                          'items' => [

                              ['label' => 'Gii', 'icon' => 'fa fa-file-code-o', 'url' => ['/gii']],
                              ['label' => 'Debug', 'icon' => 'fa fa-dashboard', 'url' => ['/site/debug']],
                              ['label' => 'Login', 'url' => ['site/login'], 'visible' => Yii::$app->user->isGuest],
                              [
                                  'label' => 'Same tools',
                                  'icon' => 'fa fa-share',
                                  'url' => '#',
                                  'items' => [
                                      ['label' => 'Gii', 'icon' => 'fa fa-file-code-o', 'url' => ['/gii'],],
                                      ['label' => 'Debug', 'icon' => 'fa fa-dashboard', 'url' => ['/debug'],],
                                      [
                                          'label' => 'Level One',
                                          'icon' => 'fa fa-circle-o',
                                          'url' => '#',
                                          'items' => [
                                              ['label' => 'Level Two', 'icon' => 'fa fa-circle-o', 'url' => '#',],
                                              [
                                                  'label' => 'Level Two',
                                                  'icon' => 'fa fa-circle-o',
                                                  'url' => '#',
                                                  'items' => [
                                                      ['label' => 'Level Three', 'icon' => 'fa fa-circle-o', 'url' => '#',],
                                                      ['label' => 'Level Three', 'icon' => 'fa fa-circle-o', 'url' => '#',],
                                                  ],
                                              ],
                                          ],
                                      ],
                                  ],
                              ],

                          ]
                      ],
                ],
            ]
        );

        if( !empty(Yii::$app->user->identity) && Yii::$app->user->identity->isAdmin(Yii::$app->user->identity->getId()) ){
            echo $adminMenu;
        }else{
            echo $nonadminMenu;
        };

        ?>
    </section>

</aside>
