CREATE TABLE `announcments` (
  `id` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `title` varchar(255) DEFAULT NULL,
  `content` text DEFAULT NULL,
  `price` int(11) DEFAULT NULL,
  `rooms` int(11) DEFAULT NULL,
  `bathrooms` int(11) DEFAULT NULL,
  `parking` varchar(255) DEFAULT NULL,
  `virtual_tour` varchar(255) DEFAULT NULL,
  `plot_area` int(11) DEFAULT NULL,
  `floor_number` int(11) DEFAULT NULL,
  `url` varchar(255) NOT NULL,
  `market` varchar(255) DEFAULT NULL,
  `year` int (11) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `map_coordinate` varchar(255) DEFAULT NULL,
  `floor_area` int(11) DEFAULT NULL,
  `provider_id` int(11) DEFAULT NULL,
  `property_id` int(11) NOT NULL,
  `province_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL
);

CREATE TABLE `images` (
  `id` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `url` varchar(255) DEFAULT NULL,
  `announcment_id` int(11) NOT NULL
);

CREATE TABLE `providers` (
  `id` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `name` varchar(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL
);

CREATE TABLE `type` (
  `id` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `name` varchar(255) DEFAULT NULL
);

CREATE TABLE `property_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `name` varchar(255) DEFAULT NULL,
  `type_id` int(11) NOT NULL
);

CREATE TABLE `provinces` (
  `id` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `name` varchar(255) DEFAULT NULL
);